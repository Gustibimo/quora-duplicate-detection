# quora-duplicate-detection
Kaggle challenge to detecting duplicate questions in Quoran with natural language processing.

The goal of this competition is to predict which of the provided pairs of questions contain two questions with the same meaning. The ground truth is the set of labels that have been supplied by human experts. 